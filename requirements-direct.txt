# The order of packages is significant, because pip processes them in the order
# of appearance. Changing the order has an impact on the overall integration
# process, which may cause wedges in the gate later.

# When modifying this file `tox -e freeze-req` must be run to regenerate the requirements-frozen.txt.
kubeconfig
kubernetes
pbr
requests
chardet
urllib3
# kubeconfig
# kubernetes==29.0.0
# pbr<=5.5.1
# requests==2.23.0
# chardet>=3.0.2,<3.1.0
# urllib3>=1.21.1,<=1.25

# Openstack Caracal 2024.1
# https://releases.openstack.org/caracal/index.html
barbican==18.0.0

python-barbicanclient==5.7.0
python-keystoneclient==5.4.0

keystoneauth1==5.6.0
keystonemiddleware==10.6.0

oslo.cache==3.7.0
oslo.concurrency==6.0.0
oslo.config==9.4.0
oslo.context==5.5.0
oslo.db==15.0.0
oslo.i18n==6.3.0
oslo.log==5.5.1
oslo.messaging==14.7.2
oslo.metrics==0.8.0
oslo.middleware==6.1.0
oslo.policy==4.3.0
oslo.serialization==5.4.1
oslo.service==3.4.1
oslo.upgradecheck==2.3.0
oslo.utils==7.1.0
oslo.versionedobjects==3.3.0

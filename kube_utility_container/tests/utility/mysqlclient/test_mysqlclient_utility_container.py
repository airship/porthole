# Copyright 2020 AT&T Intellectual Property.  All other rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import time

from kube_utility_container.tests.utility.base import TestBase


class TestMysqlclientUtilityContainer(TestBase):

    @classmethod
    def setUpClass(cls):
        cls.deployment_name = cls._get_deployment_name("mysqlclient-utility")
        super(TestMysqlclientUtilityContainer, cls).setUpClass()

    def test_verify_mysql_client_is_present(self):
        """To verify mysql-client is present"""
        exec_cmd = ['utilscli', 'mysql', '-V']
        expected = 'Ver'
        result_set = self.client.exec_cmd(self.deployment_name, exec_cmd)
        self.assertIn(
            expected, result_set, 'Unexpected value for command: {}, '
            'Command Output: {}'.format(exec_cmd, result_set))

    def test_verify_readonly_rootfs(self):
        """To verify mysqlclient-utility readonly rootfs configuration"""
        failures = []
        expected = "False"
        mysqlclient_utility_pod = \
            self.client._get_utility_container(self.deployment_name)
        for container in mysqlclient_utility_pod.spec.containers:
            if expected != \
                    str(container.security_context.read_only_root_filesystem):
                failures.append(
                    f"container {container.name} is not having expected"
                    f" value {expected} set for read_only_root_filesystem"
                    f" in pod {mysqlclient_utility_pod.metadata.name}")
        self.assertEqual(0, len(failures), failures)

    def test_verify_apparmor(self):
        """To verify mysqlclient-utility Apparmor"""
        failures = []
        expected = "runtime/default"
        mysqlclient_utility_pod = \
            self.client._get_utility_container(self.deployment_name)
        for container in mysqlclient_utility_pod.spec.containers:
            annotations_common = \
                'container.apparmor.security.beta.kubernetes.io/'
            annotations_key = annotations_common + container.name
            if expected != mysqlclient_utility_pod.metadata.annotations[
                    annotations_key]:
                failures.append(f"container {container.name} belongs to pod "
                                f"{mysqlclient_utility_pod.metadata.name} "
                                f"is not having expected apparmor profile set")
        self.assertEqual(0, len(failures), failures)

    def test_verify_mysqlclient_utility_pod_logs(self):
        """To verify mysqlclient-utility pod logs"""
        date_1 = (self.client.exec_cmd(self.deployment_name,
                                       ['date', '+%Y-%m-%d %H'])).replace(
                                           '\n', '')
        date_2 = (self.client.exec_cmd(self.deployment_name,
                                       ['date', '+%b %d %H'])).replace(
                                           '\n', '')
        exec_cmd = ['utilscli', 'dbutils', 'show_databases', 'openstack']
        self.client.exec_cmd(self.deployment_name, exec_cmd)
        time.sleep(10)
        pod_logs = (self.client._get_pod_logs(self.deployment_name)). \
            replace('\n', '')
        print(pod_logs)
        if date_1 in pod_logs:
            latest_pod_logs = (pod_logs.split(date_1))[1:]
        else:
            latest_pod_logs = (pod_logs.split(date_2))[1:]
        self.assertNotEqual(0, len(latest_pod_logs),
                            "Not able to get the latest logs")

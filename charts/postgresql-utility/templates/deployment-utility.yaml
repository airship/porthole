{{/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/}}
{{- if .Values.manifests.deployment_utility }}
{{- $envAll := . }}

{{- $serviceAccountName := printf "%s" $envAll.Release.Name }}
{{ tuple $envAll "utility" $serviceAccountName | include "helm-toolkit.snippets.kubernetes_pod_rbac_serviceaccount" }}
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: {{ $serviceAccountName }}
rules:
  - apiGroups:
      - ""
    resources:
      - namespaces
      - persistentvolumeclaims
      - persistentvolumes
      - pods
      - secrets
    verbs:
      - get
      - list
      - watch
  - apiGroups:
      - ""
    resources:
      - pods/exec
    verbs:
      - create
  - apiGroups:
      - "batch"
    resources:
      - cronjobs
    verbs:
      - get
      - list
      - watch
  - apiGroups:
      - "batch"
    resources:
      - jobs
    verbs:
      - get
      - list
      - watch
      - create
      - update
      - delete
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: {{ $serviceAccountName }}
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: {{ $serviceAccountName }}
subjects:
  - kind: ServiceAccount
    name: {{ $serviceAccountName }}
    namespace: {{ $envAll.Release.Namespace }}
---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: {{ printf "%s" $envAll.Release.Name }}
  annotations:
    {{ tuple $envAll | include "helm-toolkit.snippets.release_uuid" }}
  labels:
{{ tuple $envAll "postgresql" "utility" | include "helm-toolkit.snippets.kubernetes_metadata_labels" | indent 4 }}
spec:
  replicas: {{ .Values.pod.replicas.utility }}
  selector:
    matchLabels:
{{ tuple $envAll "postgresql" "utility" | include "helm-toolkit.snippets.kubernetes_metadata_labels" | indent 6 }}
  template:
    metadata:
      name: {{ printf "%s" $envAll.Release.Name }}
      labels:
{{ tuple $envAll "postgresql" "utility" | include "helm-toolkit.snippets.kubernetes_metadata_labels" | indent 8 }}
      annotations:
        configmap-bin-hash: {{ tuple "configmap-bin.yaml" . | include "helm-toolkit.utils.hash" }}
        configmap-etc-hash: {{ tuple "configmap-etc.yaml" . | include "helm-toolkit.utils.hash" }}
{{ dict "envAll" $envAll "podName" "postgresql-utility" "containerNames" (list "postgresql-utility") | include "helm-toolkit.snippets.kubernetes_mandatory_access_control_annotation" | indent 8 }}
    spec:
{{ dict "envAll" $envAll "application" "postgresql" | include "helm-toolkit.snippets.kubernetes_pod_security_context" | indent 6 }}
      serviceAccountName: {{ $serviceAccountName }}
      affinity:
{{ tuple $envAll "postgresql" "utility" | include "helm-toolkit.snippets.kubernetes_pod_anti_affinity" | indent 8 }}
      nodeSelector:
        {{ .Values.labels.utility.node_selector_key }}: {{ .Values.labels.utility.node_selector_value }}
      containers:
        - name: postgresql-utility
{{ tuple $envAll "postgresql_utility" | include "helm-toolkit.snippets.image" | indent 10 }}
{{ tuple $envAll $envAll.Values.pod.resources.postgresql_utility | include "helm-toolkit.snippets.kubernetes_resources" | indent 10 }}
{{ dict "envAll" $envAll "application" "postgresql" "container" "postgresql_utility" | include "helm-toolkit.snippets.kubernetes_container_security_context" | indent 10 }}
          command:
            - /tmp/bootstrap.sh
          env:
            - name: PATRONI_SUPERUSER_USERNAME
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.secrets.postgresql.admin }}
                  key: 'POSTGRES_USER'
            - name: PATRONI_SUPERUSER_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.secrets.postgresql.admin }}
                  key: 'POSTGRES_PASSWORD'
            - name: POSTGRESQL_HOST_PORT
              value: {{ tuple "postgresql" "internal" "postgresql" $envAll | include "helm-toolkit.endpoints.host_and_port_endpoint_uri_lookup" }}
            - name: BACKUP_RESTORE_SCOPE
              value: "postgresql"
            - name: BACKUP_RESTORE_NAMESPACE_LIST
              value: {{ .Values.conf.postgresql_backup_restore.enabled_namespaces | quote }}
            {{- if .Values.manifests.create_test_database }}
            - name: TEST_DB_NAME
              value: {{ .Values.conf.postgresql_backup_restore.test_database_name | quote }}
            {{- if .Values.conf.postgresql_backup_restore.test_database_user }}
            - name: TEST_DB_USER
              value: {{ .Values.conf.postgresql_backup_restore.test_database_user | quote }}
            {{- end }}
            {{- end }}
          volumeMounts:
            - name: postgresql-utility-bin
              mountPath: /tmp/bootstrap.sh
              subPath: bootstrap.sh
              readOnly: true
            - name: postgresql-utility-bin
              mountPath: /tmp/start.sh
              subPath: start.sh
              readOnly: true
            - name: postgresql-utility-bin
              mountPath: /usr/local/bin/utilscli
              subPath: utilscli
              readOnly: true
            - name: postgresql-utility-bin
              mountPath: /usr/local/bin/postgresql-utility-rootwrap
              subPath: postgresql-utility-rootwrap
              readOnly: true
            - name: postgresql-utility-bin
              mountPath: /usr/local/bin/dbutils
              subPath: dbutils
              readOnly: true
            - name: postgresql-utility-bin
              mountPath: /tmp/pgutils.sh
              subPath: pgutils.sh
              readOnly: true
            - name: postgresql-utility-bin
              mountPath: /tmp/pg-ondemand-job.sh
              subPath: pg-ondemand-job.sh
              readOnly: true
            {{- if .Values.manifests.create_test_database }}
            - name: postgresql-utility-bin
              mountPath: /tmp/create_test_database.sh
              subPath: create_test_database.sh
              readOnly: true
            {{- end }}
            - name: postgresql-utility-sudoers
              mountPath: /etc/sudoers.d/utilscli-sudo
              subPath: utilscli-sudo
              readOnly: true
            - name: postgresql-etc
              mountPath: /etc/postgresql/postgresql.conf
              subPath: postgresql.conf
              readOnly: true
            - name: postgresql-utility-etc
              mountPath: /etc/postgresql/rootwrap.d/postgresql-rootwrap.filter
              subPath: postgresql-rootwrap-filter
              readOnly: true
            - name: postgresql-utility-etc
              mountPath: /etc/postgresql/rootwrap.conf
              subPath: rootwrap.conf
              readOnly: true
{{- if .Values.pod.mounts.postgresql.container.postgresql_utility.volumeMounts }}
{{ .Values.pod.mounts.postgresql.container.postgresql_utility.volumeMounts | toYaml | indent 12 }}
{{- end }}
      volumes:
        - name: postgresql-utility-sudoers
          configMap:
            name: {{ printf "%s-%s" $envAll.Release.Name "sudoers" }}
            defaultMode: 0644
        - name: postgresql-utility-bin
          configMap:
            name: {{ printf "%s-%s" $envAll.Release.Name "bin" }}
            defaultMode: 0555
        - name: postgresql-utility-etc
          configMap:
            name: {{ printf "%s-%s" $envAll.Release.Name "etc" }}
            defaultMode: 0555
        - name: postgresql-etc
          secret:
            secretName: {{ .Values.conf.postgresqlconf.etc }}
            defaultMode: 0444
        - name: postgresql-admin
          secret:
            secretName: {{ .Values.secrets.postgresql.admin | quote }}
            defaultMode: 0444
{{- if .Values.pod.mounts.postgresql.container.postgresql_utility.volumes }}
{{ .Values.pod.mounts.postgresql.container.postgresql_utility.volumes | toYaml | indent 8 }}
{{- end }}
{{- end }}
